<?php

namespace Blocktogo\ClientApi;

use Blocktogo\ClientApi\Contracts\PeruriConfig;
use Blocktogo\ClientApi\Exceptions\InvalidSignatureException;
use Symfony\Component\HttpFoundation\Request;

class NotificationHandler
{
    protected $config;

    protected $request;

    /**
     * @var array
     */
    protected $requestContent;

    public function __construct(PeruriConfig $config)
    {
        $this->config = $config;
    }

    public function handle(Request $request): void
    {
        $signature = $request->headers->get('X-Request-Signature');
        $payload = $request->toArray();
        $timestamp = array_key_exists('timestamp', $payload) ? $payload['timestamp'] : null;

        $this->verifyRequest($signature, $timestamp);

        $this->requestContent = $payload;
    }

    protected function verifyRequest(?string $userSignature, ?string $timestamp): void
    {
        if (!$userSignature) {
            throw new InvalidSignatureException('Request tidak memiliki X-Request-Signature header.');
        }
        if (!$timestamp) {
            throw new InvalidSignatureException('Request body tidak mengandung properti timestamp.');
        }

        $secretKey = $this->config->getApiKey(); // Peruri client APIKey
        $signature = hash_hmac('sha256', $timestamp, $secretKey);
        $equals = hash_equals($userSignature, $signature);

        if (!$equals) {
            throw new InvalidSignatureException('Request signature tidak valid.');
        }
    }

    public function getRequestContent(): array
    {
        return $this->requestContent;
    }
}