<?php

namespace Blocktogo\ClientApi;

use Blocktogo\ClientApi\Contracts\PeruriConfig;

class Config implements PeruriConfig
{
    const SANDBOX_BASEURL = 'http://sandbox-api.blocktogo.id:9094';
    const PRODUCTION_BASEURL = 'https://api.blocktogo.id';

    const ACTION_UPLOAD_OCR_B64 = 'uploadOcrB64';

    protected $systemId;

    protected $apiKey;

    protected $productionEnv;

    protected $uriPaths;

    public function __construct(string $systemId, string $apiKey, bool $productionEnv = false)
    {
        $this->systemId = $systemId;
        $this->apiKey = $apiKey;
        $this->productionEnv = $productionEnv;
    }

    protected function loadUriPaths($productionEnv)
    {
        $configFile = implode(DIRECTORY_SEPARATOR, ['..', 'config', 'uripaths.php']);
        $uriPaths = require(__DIR__ . DIRECTORY_SEPARATOR .  $configFile);

        return $uriPaths[$productionEnv ? 'production' : 'sandbox'];
    }

    public function isProduction(): bool
    {
        return $this->productionEnv;
    }

    public function getSystemId(): string
    {
        return $this->systemId;
    }

    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    public function getBaseUrl(): string
    {
        return $this->isProduction()
            ? self::PRODUCTION_BASEURL : self::SANDBOX_BASEURL;
    }

    public function getUrl(string $action): string
    {
        $path = $this->uriPath($action);
        $path = trim($path, '\ ');

        return $this->getBaseUrl() . '/' . $path;
    }

    protected function uriPath(string $action): string
    {
        return $action;
    }
}