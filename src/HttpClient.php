<?php

namespace Blocktogo\ClientApi;

use Blocktogo\ClientApi\Contracts\PeruriConfig;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class HttpClient
{
    protected $config;

    protected $client;

    public function __construct(PeruriConfig $config, Client $client)
    {
        $this->config = $config;
        $this->client = $client;
    }

    protected function buildRequestOptions(array $parameters = [], bool $auth = true): array
    {
        if ($auth) {
            $authorizationHeader = 'Bearer ' . $this->prepareJWT();
        }
        else {
            $authorizationHeader = null;
        }

        $parameters = array_merge(['systemId' => $this->config->getSystemId()], $parameters);
        $options = [
            RequestOptions::JSON => ['param' => $parameters],
            RequestOptions::HEADERS => [
                'Content-Type' => 'application/json',
                'x-Gateway-APIKey' => $this->config->getApiKey(),
                'Authorization' => $authorizationHeader,
            ],
        ];

        Log::info(json_encode($options), ['context' => 'requestPayload']);

        return $options;
    }

    protected function prepareJWT(): string
    {
        if (!Cache::has('peruri-jwt')) {
            $response = $this->getJsonWebToken();
            $contents = $response->getBody()->getContents();

            $payload = json_decode($contents);
            $peruriJwt = $payload->data->jwt;
            $expirationTime = Carbon::createFromTimestamp($payload->data->expiredDate)
                ->subHour();
            Cache::put('peruri-jwt', $peruriJwt, $expirationTime);
        }
        else {
            $peruriJwt = Cache::get('peruri-jwt');
        }

        return $peruriJwt;
    }

    public function getJsonWebToken()
    {
        $result = $this->client->request(
            'POST',
            $this->config->getUrl('gateway/jwtSandbox/1.0/getJsonWebToken/v1'),
            $this->buildRequestOptions([], false)
        );

        return $result;
    }

    public function uploadOcrKtp(array $params)
    {
        $result = $this->client->request(
            'POST',
            $this->config->getUrl('gateway/digitalSignatureFullJwtSandbox/1.0/trust/uploadOcrB64/v1'),
            $this->buildRequestOptions($params)
        );

        return $result;
    }

    public function retrieveKtpData(array $params)
    {
        $result = $this->client->request(
            'POST',
            $this->config->getUrl('gateway/digitalSignatureFullJwtSandbox/1.0/trust/getDataOcr/v1'),
            $this->buildRequestOptions($params)
        );

        return $result;
    }

    public function registration(array $params)
    {
        $result = $this->client->request(
            'POST',
            $this->config->getUrl('gateway/digitalSignatureFullJwtSandbox/1.0/registration/v1'),
            $this->buildRequestOptions($params)
        );

        return $result;
    }

    public function videoVerification(array $params)
    {
        $result = $this->client->request(
            'POST',
            $this->config->getUrl('gateway/digitalSignatureFullJwtSandbox/1.0/videoVerification/v1'),
            $this->buildRequestOptions($params)
        );

        return $result;
    }

    public function checkCertificate(array $params)
    {
        $result = $this->client->request(
            'POST',
            $this->config->getUrl('gateway/digitalSignatureFullJwtSandbox/1.0/checkCertificate/v1'),
            $this->buildRequestOptions($params)
        );

        return $result;
    }

    public function sendSpeciment(array $params)
    {
        $result = $this->client->request(
            'POST',
            $this->config->getUrl('gateway/digitalSignatureFullJwtSandbox/1.0/sendSpeciment/v1'),
            $this->buildRequestOptions($params)
        );

        return $result;
    }
}