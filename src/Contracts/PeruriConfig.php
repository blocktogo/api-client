<?php

namespace Blocktogo\ClientApi\Contracts;

interface PeruriConfig
{
    public function getSystemId(): string;

    public function getApiKey(): string;

    public function getBaseUrl(): string;

    public function getUrl(string $path): string;
}