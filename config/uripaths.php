<?php

use Blocktogo\ClientApi\Config;

return [
    'production' => [

    ],
    'sandbox' =>  [
        Config::ACTION_UPLOAD_OCR_B64
            => 'gateway/digitalSignatureFullJwtSandbox/1.0/trust/uploadOcrB64/v1',
    ],
];